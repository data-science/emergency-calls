begin;

-- drop any tables
drop table calls_csv CASCADE;
drop table response_csv CASCADE;
drop table response CASCADE;

drop table calls CASCADE;
drop table call_type CASCADE;
drop table call_category CASCADE;
drop table call_group CASCADE;

drop table agency CASCADE;
drop table agency_type CASCADE;

drop table place CASCADE;

commit;
