begin;

-- -- Create the raw data tables
create table calls_csv (call_id char(12) primary key, 
date date, time time without time zone, complaint varchar, 
address varchar, city varchar, lat real, long real );

create table response_csv (call_id char(12), abbrev varchar, name varchar);

-- -- Create the core tables
create table calls (call_id int primary key, 
place_id real, date date, time time, type_name char(12));

create table place (place_id serial primary key, 
addr varchar, lat real, long real, city varchar, state varchar);

create table agency (agency_id serial unique, agency_abbrev varchar, agency_name varchar, city varchar);

create table response (call_id int references calls(call_id), agency_id int references agency(agency_id));

-- -- agency hierarchy
create table agency_type (agency_id serial unique, type_name char(15));

-- -- incident hierarchy
create table call_type (complaint_name varchar, type_name char(12), category_id real);

create table call_group (group_id serial unique, group_name varchar);
insert into call_group (group_name) values ('ems');
insert into call_group (group_name) values ('fire');
insert into call_group (group_name) values ('law');
insert into call_group (group_name) values ('misc');

create table call_category (cat_id serial unique, cat_name varchar, group_id real);
insert into call_category (cat_name) values ('ems-1');
insert into call_category (cat_name) values ('ems-2');
insert into call_category (cat_name) values ('fire-1');
insert into call_category (cat_name) values ('fire-2');
insert into call_category (cat_name) values ('law-1');
insert into call_category (cat_name) values ('law-2');
insert into call_category (cat_name) values ('misc');

update call_category set group_id = call_group.group_id from call_group where call_group.group_name='fire' and cat_name like 'fire%';
update call_category set group_id = call_group.group_id from call_group where call_group.group_name='ems' and cat_name like 'ems%';
update call_category set group_id = call_group.group_id from call_group where call_group.group_name='law' and cat_name like 'law%';
update call_category set group_id = call_group.group_id from call_group where call_group.group_name='misc' and cat_name like 'misc%';

-- -- create any useful views
create view responding_agencies as (select response.call_id, agency.agency_name from response inner join agency on response.agency_id=agency.agency_id);

create view type_cat_group as (select call_type.complaint_name,call_type.type_name,call_category.cat_name,call_group.group_name from call_type inner join call_category on call_type.category_id=call_category.cat_id inner join call_group on call_category.group_id=call_group.group_id);

create view calls_cities as (select calls.call_id,place.city from calls inner join place on calls.place_id=place.place_id);

create view latlongs as (select place.lat, place.long from calls inner join response on calls.call_id=response.call_id inner join agency on response.agency_id = agency.agency_id inner join call_type on calls.type_name = call_type.type_name inner join place on calls.place_id=place.place_id where place.lat is not null and place.long is not null);

commit;
