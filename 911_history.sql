/* Converts Positive longitudes to Negative. */
UPDATE calls SET longitude = (-1 * longitude) WHERE longitude > 0;
/* Updating our_complaint by collapsing the 10 code s*/
UPDATE calls SET our_complaint = '10-11' WHERE complaint LIKE '10-11%';
UPDATE calls SET our_complaint = '10-10' WHERE complaint = '10-10';
UPDATE calls SET our_complaint = '' WHERE complaint = '';
UPDATE calls SET our_complaint = '10-14' WHERE complaint = '10-14';
UPDATE calls SET our_complaint = '10-15' WHERE complaint LIKE '10-15%';
UPDATE calls SET our_complaint = '10-16' WHERE complaint LIKE '10-16%';
UPDATE calls SET our_complaint = '10-31' WHERE complaint LIKE '10-31%';
UPDATE calls SET our_complaint = '10-37' WHERE complaint LIKE '10-37%';
UPDATE calls SET our_complaint = '10-50' WHERE complaint LIKE '10-50%';
UPDATE calls SET our_complaint = '10-52' WHERE complaint LIKE '10-52%';
UPDATE calls SET our_complaint = '10-53' WHERE complaint LIKE '10-53%';
UPDATE calls SET our_complaint = '10-58' WHERE complaint LIKE '10-58%';
UPDATE calls SET our_complaint = '10-59' WHERE complaint LIKE '10-59%';
UPDATE calls SET our_complaint = '10-70' WHERE complaint LIKE '10-70%';
UPDATE calls SET our_complaint = '10-97' WHERE complaint LIKE '10-97%';
/* Craigles did something here */
CREATE TEMPORARY TABLE temptable (call character(20), abb character(5), name character(50));
\copy temptable (call, abb, name) from '/home/datascience/departmentsdispatched.csv' with (format csv);
/* Craigles did something here */
\copy temptable (call, abb, name) from '/cluster/home/datascience/departmentsdispatched.csv' with (format csv);
CREATE TEMPORARY TABLE temptable (call character(20), abb character(5), name character(50));
\copy temptable (call, abb, name) from '/cluster/home/datascience/departmentsdispatched.csv' with (format csv);
insert into test (agency_abbrev, agency_name) select abb, name from temptable;
/* Craigles did something here */
DROP TABLE temptable;
\copy call_details FROM '/cluster/home/datascience/departmentsdispatched.csv' WITH DELIMITER (',') CSV;
\copy call_details FROM '/cluster/home/datascience/departmentsdispatched.csv' WITH DELIMITER (',') CSV;
\copy call_details FROM '/cluster/home/datascience/departmentsdispatched.csv' CSV;
/* Make another copy of the table calls*/
create table test as select * from calls;

alter table test add our_city char(20);
/* Updating our_city column which has correct city, or county when the city is not in Wayne County(or State in case of Ohio). */
update test set city = 'Richmond' where city = 'RCHMOND';
update test set our_city = 'Richmond' where lower(city) like '%ichm%';
update test set our_city = 'Richmond' where lower(city) = 'rchmond';
update test set our_city = 'Randolph county', out_of_jurisdiction = true where lower(city) like 'rando%'; -- out_of_jurisdiction is set to True for cities out of Wayne County
update test set our_city = 'Rush county', out_of_jurisdiction = true where lower(city)='rushville';
update test set our_city = 'Randolph county', out_of_jurisdiction = true where lower(city)='ridgeville';
update test set our_city = 'Greens Fork' where lower(city) like 'greens%';
update test set our_city = 'Fayette county', out_of_jurisdiction = true where lower(city) like 'fayette%';
update test set our_city = 'Fayette county', out_of_jurisdiction = true where lower(city) like 'connersville%';
update test set our_city = 'Randolph county', out_of_jurisdiction = true where lower(city) = 'union city';
update test set our_city = 'Union county', out_of_jurisdiction = true where lower(city) = 'union co';
update test set our_city = 'Union county', out_of_jurisdiction = true where lower(city) = 'union twp';
update test set our_city = 'Fayette county', out_of_jurisdiction = true where lower(city) like 'connorsville%';
update test set our_city = 'Ohio', out_of_jurisdiction = true where lower(city) like 'new p%';
update test set our_city = 'Henry county', out_of_jurisdiction = true where lower(city) like 'new l%';
update test set our_city = 'Henry county', out_of_jurisdiction = true where lower(city) like 'new c%';
update test set our_city = 'Hagerstown' where lower(city) like 'hage%';
update test set our_city = 'Greens Fork' where lower(city) like 'green fork';
update test set our_city = 'Cambridge city' where lower(city) like 'cambridge%';
update test set our_city = 'Johnson county' where lower(city) like 'franklin%';
update test set our_city = 'Franklin county' where lower(city) = 'whitewater';
update test set out_of_jurisdiction = true  where lower(city) like 'franklin%';
update test set out_of_jurisdiction = true  where lower(city) = 'whitewater';
update test set our_city = 'Marion county', out_of_jurisdiction = true where lower(city) = 'indianapolis';
update test set our_city = 'Boston' where lower(city) like 'bos%';
update test set our_city = 'Henry county', out_of_jurisdiction = true where lower(city) = 'henry co';
update test set our_city = 'Randolph county', out_of_jurisdiction = true where lower(city) like 'white r%';
update test set our_city = 'Randolph county', out_of_jurisdiction = true where lower(city) like 'win%';
update test set our_city = 'Abington' where lower(city) like 'abing%';
update test set our_city = 'Bethel' where lower(city) like 'bethel%';
update test set our_city = 'Centerville' where lower(city) like 'center%';
update test set our_city = 'Dublin' where lower(city) = 'dublin';
update test set our_city = 'Mt Auburn' where lower(city) = 'mt auburn';
update test set our_city = 'Webster' where lower(city) = 'webster';
update test set our_city = 'Richmond' where lower(city) = 'q';
update test set our_city = 'Richmond' where lower(city) like 'cause%';
update test set our_city = 'Muncie' where lower(city) = 'sweetwater';
update test set our_city = 'Centerville' where lower(city) = 'cnet';
update test set our_city = 'Economy' where lower(city) = 'econd';
update test set our_city = 'Richmond' where lower(address) = '18 n 22nd st';
/* Make a csv file for the addresses which do not have latitudes and are not to be ommitted. This file is then used by the QGIS for geocoding. */
\copy (select address, our_city from test where latitude is null and complaint_category <> 'o') to 'lat_long_find.csv' with csv delimiter ',';
