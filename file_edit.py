read_file = open(your_file,'r+')

i = 0
all_lines = read_file.readlines()
read_file.seek(0)
for line in all_lines:
    line = line.rstrip()
    if i == 0:
        line += ",State"
        print(line, file=read_file)
    else:
        line += ", IN"
        print(line, file=read_file)
    i+=1

read_file.truncate()
read_file.close()