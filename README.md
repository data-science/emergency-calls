# About

This is the code and some other documentation for the 911 calls project.

# Key Files

This is where we are going to list key files.
* file_edit.py: edits the file
* final_map.py: creates the final visualization
* 911_history.sql: an SQL history file

# Project Members

This project is conducted by Shana Weissman, Craig Earley and Ahsan Ali Khoja.

