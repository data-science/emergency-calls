-- Add schema from database for reference

begin;

-- -- Populate them from the raw and uncleaned csv
\copy calls_csv from '/cluster/home/cjearley13/DataScience/emergency-calls/2015calls.csv' with delimiter ',' csv header;
\copy response_csv from '/cluster/home/datascience/911calls/raw/departmentsdispatched.csv' with delimiter',' csv;
--\copy response_csv from '/cluster/home/cjearley13/DataScience/emergency-calls/2016departmentsdispatched.csv' with delimiter ',' csv header;

-- set to initcap so as to make life easier
update calls_csv set city=initcap(city);

-- -- do something to get the additional geocodes we need: postgis? clean up, geocode

-- Make cities consistent and correct
update calls_csv set city= 'Richmond' where
city like 'Rich%' or
city like '%chmond' or
city= 'Q' or
city= 'Wtt861' or
address like 'RPD' or
(address like '18 N 22ND ST' and city like ' %') or
city like '1 S 9th%';

update calls_csv set city= 'Greens Fork' where city like 'Green%ork';
update calls_csv set city= 'Hagerstown' where city like 'Hage%';
update calls_csv set city= 'Boston' where city like 'Bost%';
update calls_csv set city= 'Connersville' where city like 'Connor%';
update calls_csv set city= 'Cambridge City' where city like 'Cambr%';
update calls_csv set city= 'New Paris' where city like 'New Paris%';
update calls_csv set city= 'Union City' where city like 'Union%';
update calls_csv set city= 'Randolph County' where city like 'Rando%';
update calls_csv set city= 'Fayette County' where city like 'Fayette%';
update calls_csv set city= 'Economy' where city like 'Econ%';
update calls_csv set city= 'Centerville' where city like 'Cnet';
update calls_csv set city= 'Farmland Area' where city like 'Farmland%';

-- Some things that are generic enough to update to Wayne County alone
update calls_csv set city= 'Wayne County' where
city= 'Your City' or city= 'Unk' or city= 'Pu';

-- Very specific problems
update calls_csv set city= 'Richmond' where address like '%SW 18TH%';
update calls_csv set address= 'Bertschland, 1 E Church St' where city= '1 E Church St';
update calls_csv set city= 'Cambridge City' where city= '1 E Church St';

-- -- Modify the response_csv table
update response_csv set name=initcap(name);
update response_csv set name= 'Negotiators' where name like 'Negg%';

-- -- if no errors, insert the clean data into the appropriate postgres tables 

-- -- spread the data downstream to the derived tables

-- Take care of calls_csv
-- want to use property cards for the ID over time
-- create table place (place_id serial unique, addr varchar, lat real, long real, city varchar, state varchar);
\copy place (lat, long, addr, city, state) from '/cluster/home/cjearley13/DataScience/emergency-calls/new.csv' with delimiter',' csv header;
insert into place (addr, lat, long, city) select distinct address, lat, long, city from calls_csv;
-- -- locations are processed, now use that

insert into calls (call_id, place_id, date, time, type_name)
select distinct on (calls_csv.call_id) RIGHT(calls_csv.call_id,5)::integer, place.place_id, calls_csv.date, calls_csv.time, calls_csv.complaint from calls_csv inner join place on place.addr = calls_csv.address;

-- -- incident hierarchy
insert into call_type (complaint_name, type_name) select distinct upper(complaint), upper(complaint) from calls_csv;

-- -- clean up the complaints
update call_type set type_name = '10-15' where complaint_name like '10-15%';
update call_type set type_name = '10-16' where complaint_name like '10-16%';
update call_type set type_name = '10-31' where complaint_name like '10-31%';
update call_type set type_name = '10-37' where complaint_name like '10-37%';
update call_type set type_name = '10-50' where complaint_name like '10-50%';
update call_type set type_name = '10-52' where complaint_name like '10-52%';
update call_type set type_name = '10-53' where complaint_name like '10-53%';
update call_type set type_name = '10-58' where complaint_name like '10-58%';
update call_type set type_name = '10-59' where complaint_name like '10-59%';
update call_type set type_name = '10-70' where complaint_name like '10-70%';
update call_type set type_name = '10-97' where complaint_name like '10-97%';
update call_type set type_name = '10-11' where complaint_name like '10-11%';
update call_type set type_name = '10-90' where complaint_name like '10-90%';

update call_type set category_id = call_category.cat_id from call_category where call_category.cat_name like 'law%' and
(type_name like '10-10%' or 
type_name like '10-11%' or 
type_name like '10-14%' or 
type_name like '10-15%' or 
type_name like '10-16%' or 
type_name like '10-31%' or 
type_name like '10-32%' or 
type_name like '10-37%' or 
type_name like '10-38%' or 
type_name like '10-45%' or 
type_name like '10-53%' or 
type_name like '10-55%' or 
type_name like '10-56%' or 
type_name like '10-58%' or 
type_name like '10-59%' or 
type_name like '10-70%' or 
type_name like '10-90%' or 
type_name like '10-94%' or 
type_name like '10-96%' or 
type_name like '10-97%' or 
type_name like 'ARTICLE RECV%' or 
type_name like 'AUDIBLE ALRM%' or 
type_name like 'BAR CHECK%' or 
type_name like 'BIKE PICKUP%' or 
type_name like 'BUSINESS ALM%' or 
type_name like 'BUSINESS CK%' or 
type_name like 'CHILD ABUSE%' or 
type_name like 'CHILD NEGLEC%' or 
type_name like 'DRIVE OFF%' or 
type_name like 'EMER DETENT%' or 
type_name like 'EXTRA ATTEN%' or 
type_name like 'FALSE INFORM%' or 
type_name like 'FLAGGED DOWN%' or 
type_name like 'FOUND ARTCLE%' or 
type_name like 'GRAFFITI%' or 
type_name like 'HARASSMENT%' or 
type_name like 'HOLDUP ALARM%' or 
type_name like 'HOMICIDE%' or 
type_name like 'INCORIG JUV%' or 
type_name like 'INV%' or 
type_name like 'INV BAT ADLT%' or 
type_name like 'INV BAT JUV%' or 
type_name like 'INV B&E BUS%' or 
type_name like 'INV B&E OTH%' or 
type_name like 'INV B&E RES%' or 
type_name like 'INV CRIM REC%' or 
type_name like 'INV FORGERY%' or 
type_name like 'INV FRAUD%' or 
type_name like 'INV INVASION%' or 
type_name like 'INV MISCHIEF%' or 
type_name like 'INV ROBBERY%' or 
type_name like 'INV THEFT%' or 
type_name like 'INV VEH THFT%' or 
type_name like 'JUV PROBLEM%' or 
type_name like 'KIDNAP/ABDUC%' or 
type_name like 'LOCK OUT%' or 
type_name like 'LOST PLATE%' or 
type_name like 'MISSING PER%' or 
type_name like 'OPEN BUILDNG%' or 
type_name like 'OPEN VEHICLE%' or 
type_name like 'PEACE OFFICR%' or 
type_name like 'PROSTITUTION%' or 
type_name like 'PURSE SNATCH%' or 
type_name like 'PURSUIT FOOT%' or 
type_name like 'PURSUIT VEH%' or 
type_name like 'RECKLESS DRV%' or 
type_name like 'RESIDENTL AL%' or 
type_name like 'ROAD RAGE%' or 
type_name like 'RUNAWAY%' or 
type_name like 'RUNAWAY RET%' or 
type_name like 'SEARCH WRRNT%' or 
type_name like 'SEX OFFENDER%' or 
type_name like 'SHOOTING%' or 
type_name like 'SHOPLIFTER A%' or 
type_name like 'SHOPLIFTER J%' or 
type_name like 'SHOTS FIRED%' or 
type_name like 'SIG 23%' or 
type_name like 'SIG 27%' or 
type_name like 'SIG 60%' or 
type_name like 'SIREN%' or 
type_name like 'SLIDE OFF%' or 
type_name like 'SPEAK W/OFF%' or 
type_name like 'SPECIAL EVNT%' or 
type_name like 'STABBING%' or 
type_name like 'STALKING%' or 
type_name like 'SUICIDAL SUB%' or 
type_name like 'TRAFFIC DTL%' or 
type_name like 'Traffic Stop%' or 
type_name like 'TRAFFIC STOP%' or 
type_name like 'TRANSPORT%' or 
type_name like 'TRASH DUMPNG%' or 
type_name like 'TREE DOWN%' or 
type_name like 'TRESPASSING%' or 
type_name like 'TRUANT%' or 
type_name like ' UNAUTH CNTRL%' or 
type_name like 'UNKNOWN PROB%' or 
type_name like 'VACANT HOUSE%' or 
type_name like 'VEHICLE RECV%' or 
type_name like 'VIN%' or 
type_name like 'WELFARE CK%');  

update call_type set category_id = call_category.cat_id from call_category where call_category.cat_name like 'fire%' and
(type_name like '10-37 ODOR%' or
type_name like '10-50 FATAL%' or
type_name like '10-50 PI H&R%' or
type_name like '10-50 UNK%' or
type_name like '10-52 BURN%' or
type_name like '10-52 ELECTR%' or
type_name like '10-70%' or
type_name like '10-70 CARBON%' or
type_name like '10-70 DUMP%' or
type_name like '10-70 GAS%' or
type_name like '10-70 GRASS%' or
type_name like '10-70 IL BRN%' or
type_name like '10-70 SMOKE%' or
type_name like '10-70 STRUCT%' or
type_name like '10-70 TRASH%' or
type_name like '10-70 VEHICL%' or
type_name like 'ARSON%' or
type_name like 'BURN%' or
type_name like 'FIREWORKS%' or
type_name like 'HAZMAT%' or
type_name like 'WIRES DOWN%');

update call_type set category_id = call_category.cat_id from call_category where call_category.cat_name like 'misc%' and 
(type_name like '10-17%' or
type_name like '10-53%' or
type_name like '10-53 HC%' or
type_name like '10-54%' or
type_name like '10-59%' or
type_name like 'APS%' or
type_name like 'ASSIST OTHER%' or
type_name like 'BIKE%' or
type_name like 'GENERAL%' or
type_name like 'HOTEL/MOTEL%' or
type_name like 'MCAB%' or
type_name like 'MSC%' or
type_name like 'PERSNL ASST%' or
type_name like 'P&W%' or
type_name like 'REPO%' or
type_name like 'SIG 17%' or
type_name like 'UNWANTED%');

update call_type set category_id = call_category.cat_id from call_category where call_category.cat_name like 'ems%' and 
(type_name like '10-52%' or
type_name like '10-52 ALLERG%' or
type_name like '10-52 BIRTH%' or
type_name like '10-52 CP%' or
type_name like '10-52 DIABET%' or
type_name like '10-52 FALL%' or
type_name like '10-52 HEAD%' or
type_name like '10-52 HEART%' or
type_name like '10-52 LIFT%' or
type_name like '10-52 SEIZU%' or
type_name like '10-52 STROKE%' or
type_name like '10-52 TRANS%' or
type_name like '10-52 UNK%' or
type_name like 'IND EXPOSURE%' or
type_name like 'MEDICAL ALRM%' or
type_name like 'OVERDOSE%');

-- 
update call_type set category_id = call_category.cat_id from call_category where call_category.cat_name like 'misc%' and category_id is null;

-- -- Now take care of response_csv
-- -- agency processing: populate agency table, from those id's populate response table
insert into agency (agency_abbrev, agency_name) select distinct abbrev,name from response_csv;

insert into response (call_id, agency_id) select RIGHT(calls_csv.call_id,5)::integer, agency.agency_id from calls_csv inner join response_csv on response_csv.call_id=calls_csv.call_id inner join agency on response_csv.abbrev=agency.agency_abbrev;

update call_type set type_name=lower(type_name);

commit;
