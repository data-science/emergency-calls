import pandas as pd
import geoplotlib as gp

all_ = pd.read_csv('map_from_db.csv')

law_data = all_[all_.category == 'l']
ems_data = all_[all_.category == 'e']
multi_data = all_[all_.category == 'm']
fire_data = all_[all_.category == 'f']
#ques_data = all_[all_.category == '?']
        
gp.dot(law_data, color = 'blue')
gp.dot(ems_data, color = 'white')
gp.dot(multi_data, color = 'green')
gp.dot(fire_data, color = 'red')
#gp.dot(ques_data, color = 'yellow')
gp.tiles_provider("darkmatter")
gp.show()